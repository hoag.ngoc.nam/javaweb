package com.ptit.hoagnam.entity;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "m_accounts")
@Table(name = "MACCOUNT")
public class MAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long accountId;
	@Column(name = "first_name", length = 50, nullable = false, unique = false)
	private String firstName;
	@Column(name = "last_name", length = 50, nullable = false, unique = false)
	private String lastName;
	@Column(name = "mail_adr", length = 100, nullable = false, unique = true)
	private String mailAdr;
	@Column(name = "password", length = 100, nullable = false, unique = false)
	private String password;
	@Column(name = "sex", length = 1, nullable = true)
	private Integer sex;
	@Column(name = "role", length = 1, nullable = true)
	private String role;
	@Column(name = "phone", length = 15, nullable = true)
	private String phone;
	@Column(name = "birthday", nullable = true)
	private Date birthday;
	@Column(name = "address", nullable = true)
	private String address;
	@Column(name = "path_img", nullable = true)
	private String pathImg;
	@Column(name = "is_active", nullable = true)
	private String isActive;
	@Column(name = "remember_token", nullable = true)
	private String rememberToken;
	@Column(name = "del_flg", nullable = true)
	private Integer delFlg;
	@Column(name = "created_at", nullable = true)
	private Date createdAt;
	@Column(name = "updated_at", nullable = true)
	private Date updatedAt;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMailAdr() {
		return mailAdr;
	}

	public void setMailAdr(String mailAdr) {
		this.mailAdr = mailAdr;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPathImg() {
		return pathImg;
	}

	public void setPathImg(String pathImg) {
		this.pathImg = pathImg;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRememberToken() {
		return rememberToken;
	}

	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}

	public Integer getDelFlg() {
		return delFlg;
	}

	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
