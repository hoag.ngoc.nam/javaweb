package com.ptit.hoagnam.service;

import com.ptit.hoagnam.entity.MAccount;

import java.util.List;

public interface AccountService {
	public List<MAccount> getListAccount();
}
