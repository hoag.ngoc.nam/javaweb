package com.ptit.hoagnam.service.implement;

import com.ptit.hoagnam.entity.MAccount;
import com.ptit.hoagnam.repository.AccountRepository;
import com.ptit.hoagnam.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
	private final AccountRepository accountRepository;

	public AccountServiceImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public List<MAccount> getListAccount() {
		return this.accountRepository.getListAccount();
	}
}
