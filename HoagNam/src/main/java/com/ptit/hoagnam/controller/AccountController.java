package com.ptit.hoagnam.controller;

import com.ptit.hoagnam.entity.MAccount;
import com.ptit.hoagnam.service.implement.AccountServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AccountController {

	private final AccountServiceImpl accountService;

	public AccountController(AccountServiceImpl accountService) {
		this.accountService = accountService;
	}

	@GetMapping(value = "/user-list")
	public ModelAndView list() {
		ModelAndView MaV = new ModelAndView();
		MaV.setViewName("admin/account/list");
		List<MAccount> data = this.accountService.getListAccount();
		MaV.getModel().put("data", data);
		return MaV;
	}
}
