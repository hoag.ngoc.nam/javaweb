package com.ptit.hoagnam.repository;


import com.ptit.hoagnam.entity.MAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<MAccount, Long> {
	@Query(
			value = "SELECT * FROM m_accounts ma",
			nativeQuery = true)
	List<MAccount> getListAccount();
}
