-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 10, 2021 lúc 01:33 PM
-- Phiên bản máy phục vụ: 10.4.20-MariaDB
-- Phiên bản PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hoagngocnam`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cache_data`
--

CREATE TABLE `cache_data` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `maccount`
--

CREATE TABLE `maccount` (
  `account_id` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `del_flg` int(11) DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mail_adr` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `path_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_01_01_000000_create_m_accounts_table', 1),
(2, '2021_01_02_000000_create_m_shops_table', 1),
(3, '2021_01_03_000000_create_catalogs_table', 1),
(4, '2021_01_04_000000_create_products_table', 1),
(5, '2021_01_05_000000_create_banners_table', 1),
(6, '2021_01_06_000000_create_room_chats_table', 1),
(7, '2021_01_07_000000_create_room_members_table', 1),
(8, '2021_01_08_000000_create_m_messages_table', 1),
(9, '2021_05_12_153315_create_password_resets_table', 1),
(10, '2021_05_14_204914_create_cache_data_table', 1),
(11, '2021_05_14_205207_create_session_data_table', 1),
(12, '2021_06_07_105234_create_room_chat_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_accounts`
--

CREATE TABLE `m_accounts` (
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'họ và tên đệm',
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'tên',
  `mail_adr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'địa chỉ mail',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'mật khẩu',
  `sex` tinyint(4) DEFAULT NULL COMMENT '0: male ;1: female ;2: other',
  `role` tinyint(4) NOT NULL DEFAULT 1 COMMENT '9: quản lý ; 1: quản trị viên ; 0: khách hàng',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'số điện thoại',
  `birthday` date DEFAULT NULL COMMENT 'ngày sinh',
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ',
  `path_img` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'đường dẫn avatar',
  `is_active` tinyint(4) DEFAULT 0 COMMENT '0: chưa active; 1: đã active',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `m_accounts`
--

INSERT INTO `m_accounts` (`account_id`, `shop_id`, `first_name`, `last_name`, `mail_adr`, `password`, `sex`, `role`, `phone`, `birthday`, `address`, `path_img`, `is_active`, `email_verified_at`, `remember_token`, `del_flg`, `created_at`, `updated_at`) VALUES
(1, 1, 'Santiago Hoeger I', 'Mr. Freddie Schultz', 'sasha.denesik@example.net', '$2y$10$6Drpogz65JhYPIA4wuShyuFoiAwFbCdwt2M8EQBJgRRCWv7q0qELu', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(2, 1, 'Miss Madaline Murray', 'Mrs. Elouise Brown IV', 'ted.jast@example.net', '$2y$10$Jy9N.metx1iDvXf0AmWv..nq.IU4AkYAQYwYvJm04Ux1au1WTbnxe', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(3, 1, 'Solon Rohan', 'Gaston Paucek', 'shyanne.steuber@example.org', '$2y$10$VbVUuk.tsLcLzDCgh/RysuHuEaCvvQSEbFQ78nBi/gPD880Sf7Lau', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(4, 1, 'Shana Hamill', 'Angus Powlowski', 'janiya.batz@example.com', '$2y$10$iVLkO1U0Qrxenpa2I45cEu4atKJaE3FSkNbOE1Mnww2IsgqzToaFC', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(5, 1, 'Cordia Runolfsson', 'Prof. Rylan Blanda', 'weimann.jeffry@example.com', '$2y$10$4fPzqlqbyqp5BE9e9A51QeDkTB2U0./2ZK6t7tbA2CWAf8BV6lNJi', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(6, 1, 'Vernice Rippin', 'Mrs. Francisca Bashirian MD', 'heidi69@example.org', '$2y$10$/yGYIxYEv3NeEtem0bOrZuH7B5Ho9coDitYht918DZQfIA8zvsUoe', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(7, 1, 'Eugene Fay', 'Earlene Beier', 'gibson.brice@example.net', '$2y$10$nxrAgnUtmUhlowW/jTH8qOLHxTHrmd40DiSfLhCR68VEmJ0YNNuUu', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(8, 1, 'Josue Becker', 'Jonathon Will', 'price.jasper@example.com', '$2y$10$VwRIrup5m/MkeMvROpsfqe0bwy5Jl5kQfgAhF8nqzpPVAWctPsS0i', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(9, 1, 'Sonny O\'Conner', 'Estel Cole I', 'imani81@example.org', '$2y$10$RM9y/bqFIymZ1wL0IgU9v.mB7jOznQ/QotdUl5zjmo44gVpPWIgfW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(10, 1, 'Westley Fahey', 'Makenzie Paucek MD', 'mcclure.corene@example.org', '$2y$10$TgOvieOjc.LV86R7qS/q0ehzt5m33LhEgHG6lM1E0fZxkKxDH/wNq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(11, 1, 'Elmer Nikolaus', 'Mr. Graham Douglas II', 'alfonzo85@example.net', '$2y$10$TE6zDCbljpZjP5NTWMS8bu3BPHDPoi90Eg/zi8PnMkFQwygGJFdB.', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(12, 1, 'Kelvin Hoeger', 'Tracey Beahan', 'douglas.gibson@example.net', '$2y$10$5TkMJwP4cd.pi2d46KsV1.6MD9jT2V19CqkzmUKT8ag5XkI7f4Fl6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(13, 1, 'Gunnar Jacobs', 'Enola Maggio', 'stokes.cassidy@example.com', '$2y$10$GQqKntXAEFXJj4h4Abw.5uS/FIsW5.2AfppTENmL9kbridqHIKRuu', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(14, 1, 'Ms. Ima Tillman Jr.', 'Dr. Kailee Hane', 'ilangosh@example.net', '$2y$10$3Pr.HJKxYIpyLOqfzCsOLO2VUQC0stfxp7gibLn.fGKBOAcEEaEqO', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(15, 1, 'Alice D\'Amore III', 'Albert Hayes', 'federico.boehm@example.net', '$2y$10$gA1kvRO8/qnDU7xq5x9OtOhbMJt5Ys/S81c7vnFHzkGF8fkndG.3.', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(16, 1, 'Mr. Derrick Farrell III', 'Otilia Conn', 'ufunk@example.org', '$2y$10$qIvUbyedzTCd4nqGJNaaXOJ2PiFjZyTctbVqYA2MAUTeacqTmbyqy', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(17, 1, 'Lilly Johns', 'Spencer Green I', 'arnaldo76@example.com', '$2y$10$gibhTjP3HqOXy8LXjiDGSOuEzZ2kWi7Jrp0OHbIQh4yb11jJI7Yt2', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(18, 1, 'Dr. Leonora Hudson', 'Lupe Schumm MD', 'grayce63@example.net', '$2y$10$MClJzD6WvG0u50RMiUJe0uG7OppF8pPMYkN1Z9U/NhSi.bu7BgrHy', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(19, 1, 'Prof. Margaretta Turner MD', 'Diego Ernser I', 'hosea68@example.net', '$2y$10$FwOpRtczyIEeGMqXosJoNe/M/53qGWF96iXmWjZw.9AlTZtDfWvSy', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(20, 1, 'Miss Clarissa Flatley Jr.', 'Elton Ankunding', 'olson.birdie@example.com', '$2y$10$Tts8l..MLrzjwybPVAOytu3cBiW364tLzRwGCq/WjvC3zFnlbDDJW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(21, 1, 'Regan Swaniawski DDS', 'Freddie Bahringer', 'obailey@example.org', '$2y$10$Te/PsaWnolvZr2MyLriecuFtuUGdIRcKAdNlWhYjV/NkovKkRVjrW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(22, 1, 'Jaunita Stroman', 'Mr. Harvey Mohr', 'bauch.mack@example.com', '$2y$10$9JfiR8Ig2gvVzr9hI1breuiaTMXXrfKpSufoD9A1i1JfqGg9ZcbBK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(23, 1, 'Ms. Marisa Hauck III', 'Prof. Christopher Blanda', 'garrison00@example.org', '$2y$10$eu9Z6wCYAKf.Ris4ccjd9uU3MpavpPsxWI1vS3WqRvk.r/vSQ8s2.', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(24, 1, 'Eden Jakubowski II', 'Hank McCullough', 'drew.howe@example.net', '$2y$10$Ho2dkLkthrwrcEIFlScjc.kJsgyxK8Edq53X1lwI1ledtulJmGMPC', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(25, 1, 'Miss Kailyn Swift', 'Daija Grimes II', 'francisca.emmerich@example.org', '$2y$10$D9tvVnXw05Q6qFKlswlrpuAuor9i5re1M/cQPQ17YDWXZ8K6LCjH6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(26, 1, 'Dr. Marta Flatley', 'Jett Boehm', 'elmer.heidenreich@example.com', '$2y$10$K0qajEXrBvUgIzFbQ9bpi.USIgho7D85SJQw4j9pqiV9zutSkIB0a', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(27, 1, 'Lue Padberg', 'Dr. Cortez Feest', 'eleanora75@example.com', '$2y$10$QArw01Y8JOnhKzolr8quC.cAfWJ.zFIyYBOGQW95MoO0gp20XZmry', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(28, 1, 'Carole Tillman', 'Prof. Niko Mueller', 'davis.tyson@example.org', '$2y$10$.3HLhlIbvb3OcMNycC0wV.4axS9hvSTxLPNS.lQdEq7P5CjXozp0O', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(29, 1, 'Mrs. Freeda Durgan PhD', 'Jean Nicolas', 'sokon@example.com', '$2y$10$hxfoC0PibwdlWdtj/pVlyewn7CT.COH5iQ6K9aqaWUNyScRpDPZtG', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(30, 1, 'Maverick Kilback', 'Clemmie Fahey', 'mireille45@example.org', '$2y$10$dwVjqMzzt3vHEeaZ4x/l3.BKHsissJXHsW59HxQehSpQjZ0tAUS4q', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(31, 1, 'Rashawn Dicki', 'Bernie Willms III', 'xkris@example.com', '$2y$10$iHQBgDzsWs3QEXnODB4fr.L3pvtFlbDPrz7bahTjSIiOKgJUSDZE2', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(32, 1, 'Cristal Krajcik DDS', 'Jeanne Ryan', 'astrid34@example.org', '$2y$10$AyajxDrbVIvHHbkVgkp3YeLe.cQaqIGMGYyqXEpot6LzfRlDqa87m', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(33, 1, 'Mrs. Yadira Keebler', 'Norval Mohr', 'vlangworth@example.net', '$2y$10$7ZQ1ucacqt6wKCGL/zuX5ek.Ggomg1xIvyyrtkq9mYSqFFV5KjrrG', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(34, 1, 'Miss Beryl Schmeler', 'Jovany Klocko I', 'lhintz@example.net', '$2y$10$Y4t02ElJ80sy/K5X0QuF3.WUimKUXDDZrNrx4q0Ec.o27IQObiuwW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(35, 1, 'Marietta Wisoky', 'Mark Moen', 'hfisher@example.org', '$2y$10$tIYwpeC9xQFKIdGaCGkYUOePctyVKuHUik0E6YfFK/LGx3.ucHkN6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(36, 1, 'Johnpaul Upton PhD', 'Prof. Electa Stokes', 'abartell@example.com', '$2y$10$5rjv2JqNtBhofnex4X85F.0Ks29pYMIqIDLgGSzLGWrl/Oov/u1TW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(37, 1, 'Rahul Witting', 'Jannie Berge', 'stroman.derick@example.org', '$2y$10$devW.gskwPMlFMRr59bfqOfKiUcY3zNBk7IT2NVQtymCI7SfLwwgy', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(38, 1, 'Jessika Ullrich', 'Prof. Cade Parisian', 'lane.connelly@example.net', '$2y$10$xs2ggUKzSObAtChU2HpVUeWUs9oz2.xAMr4EGfXdaZDgoT.fXl1ym', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(39, 1, 'Joan Donnelly', 'Ms. Carmen Hand', 'connelly.karina@example.net', '$2y$10$zknEsD0BO0Zx7lf7Den81OT4ombm0b9enp1ZuMRbt8k8W22mLth6u', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(40, 1, 'Abagail Lesch', 'Obie Fritsch III', 'shyann.brekke@example.net', '$2y$10$i/lWCuhF3GOY1g2dk8kZ7uWtAPfDs4LEfUDyP5t.v1XdpYbJ.a9iS', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(41, 1, 'Alessandro Turcotte', 'Dr. Colten Prohaska Jr.', 'gerda47@example.org', '$2y$10$PKMXlo7qkMFQg4tOV6SS3u48rV.7.xV01kAKTF2xV9aCt2/vq7Q8G', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(42, 1, 'Layne Champlin', 'Enrique Grady', 'gbosco@example.com', '$2y$10$mCONJVcbb94aHP7Xn8ACCOTTUKabCkq.mddwtMgE01oweXiiZaGDG', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(43, 1, 'Sydney Hermiston', 'Sylvia Wehner', 'spinka.colten@example.org', '$2y$10$a4SgQbsiMYcRMm3aqLyzkul/yOIQQXb66FoNYXLKrWTyAxQYutc9O', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(44, 1, 'Abe Marquardt', 'Bart Brekke', 'schowalter.otto@example.com', '$2y$10$v/oIE0qebEU/VRcVZpJ4JOTOXKi397qYjm8Qxt3jdaGDWAdvC.TnO', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(45, 1, 'Theodore Welch MD', 'Kamren Botsford', 'presley.reichert@example.com', '$2y$10$54xyGpPwIXP3M22LUSXuhOarUZTamhqEuQogxfdxWyxSCU/Ro5CwW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(46, 1, 'Dr. Kenyatta Shields I', 'Dolores Jerde IV', 'leslie.roberts@example.org', '$2y$10$8BvzAkt8W5B0TyryK5psVuWVT3pNDzq0c9d9RxbrtC.xeQA04bWkS', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(47, 1, 'Ms. Janie Erdman', 'Aylin DuBuque', 'rylee.nikolaus@example.net', '$2y$10$CPKiFH0xNaUs.7ITCk9kreupLCbHUac.Nix7Vh91QWbut/XZKTtsi', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(48, 1, 'Mercedes Lockman', 'Prof. Luther Casper V', 'hills.andy@example.org', '$2y$10$zqin7HGVpYgMRLdc5w2GWufYMxi3ypp1X4FPHhOTIf8qPwMtdMdF6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(49, 1, 'Gwendolyn Lindgren', 'Alycia Abernathy', 'josianne.hessel@example.net', '$2y$10$/eVj9BSj3c3ueX2Xiq30neVMULtGOCMiqhvQnZIjQc9m3e0lVTSh2', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(50, 1, 'Mazie Lowe', 'Luigi DuBuque', 'raina29@example.net', '$2y$10$Eg3ez1cS6tSWTyCAXNF0vO.QaA4dV.hchIkByBmTEcqVdUdWwuZ06', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(51, 1, 'Reina Witting', 'Janice Tremblay', 'howell45@example.com', '$2y$10$7AblYzBzMtj43D2jMMXk5.RyOkNTBkOEFGSGVdFbVnFlb9mRPfnL.', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(52, 1, 'Arno Sawayn', 'Fern Hessel DVM', 'utreutel@example.com', '$2y$10$7F3JCfOuc5AuTneAR9zdwechgpdZDWKm0cy1DnZQYoXWJZuNGVRZm', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(53, 1, 'Mrs. Ethelyn Rutherford I', 'Mr. Manuela Wuckert MD', 'paufderhar@example.org', '$2y$10$jl137q2RscquKqBOdUTiuuLwkNPJmjiWjROSDF36zT8LengJWQ466', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(54, 1, 'Michaela Heaney', 'Mr. Davonte Kulas I', 'uokon@example.com', '$2y$10$UC0qxma78hSPZxzL1jX1Qukr7nEEWheKrWgdYyNII8VJZjXGnGJ5O', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(55, 1, 'Dan Hamill', 'Dasia Rutherford MD', 'richard28@example.com', '$2y$10$6VCY8kGzpNvyiOp8tCdzWe0dIfYoCvvTVyxtjTpPiWwDQ2dwuY5FO', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(56, 1, 'Roger Glover', 'Lambert Fahey DDS', 'nelle17@example.net', '$2y$10$aLj/t2b/loXH0isHzmRV1e0IESP9g6XwR40ywf/R0ecb.6OiGUcle', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(57, 1, 'Zaria Gutmann', 'Mr. Alec McCullough', 'layla36@example.net', '$2y$10$XX0Gdqmwk5XSPaZQZ7iL6.rNmUDJqGGGOH2JHurpQaa56d71Ak6Oq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(58, 1, 'Emelie Feest III', 'Royal Nader', 'trent56@example.net', '$2y$10$ADDnHZTlJoDJoOPXi8cPs.N0MBoDrN3cBmcnk1eh9Ne2IeowgCWcG', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(59, 1, 'Garret Jones PhD', 'Dr. Lisandro Fritsch V', 'cullen.bashirian@example.net', '$2y$10$I2zasQd3BksErX8dhAMzKeAFeipbZjgA1DjH9H38l.oITr1AI/.xa', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(60, 1, 'Katarina Orn', 'Zena Lind V', 'margie58@example.net', '$2y$10$KtutrhFurzDiXdh.0oD9Kel3Vr8lmk1kw7rAc1VxgE34180VJ4o3a', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(61, 1, 'Eve Huel', 'Bettye Roob', 'santina97@example.org', '$2y$10$5JYNCqGFk5j3MhBNtpa8pe3BdqqLEMIw2vIL5FJK4PSbg.Kke7UlK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(62, 1, 'Lenna Dietrich', 'Prof. Norris Renner', 'kallie97@example.org', '$2y$10$1mmPN2VH5aZKNVEu3PrHJuGXYz17PxhsBUDL8LauNFso0JNj1HnDG', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(63, 1, 'Seamus Cummerata', 'Lina Feest IV', 'stuart86@example.org', '$2y$10$WBXCfPofZSdMVTuNTPOEEOrWuO7MPzHiryqsKFUEu5ADzMk4SijhW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(64, 1, 'Fae Miller', 'Esther Sanford', 'bgibson@example.net', '$2y$10$LWGcMEzukMgI2Gdo7YOcKe.YqMPF7Tv4gCpCczXqYJLLqoDqwS0Di', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(65, 1, 'Dr. Herta Hessel', 'Brant Hagenes', 'ihalvorson@example.com', '$2y$10$ncWOh8rWYypRA0QxPsw1Vu1vnkxunLWtbF1AK/h42pNFf/jVWGj66', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(66, 1, 'Prof. Jarred Reynolds DVM', 'Seth Carter', 'ysmith@example.com', '$2y$10$RJHenHe1uzG5w7BbXMISE.5jaF1lmOiZ85/XJ8kn5Sfxbr//cgnwK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(67, 1, 'Prof. Freeman VonRueden', 'Moses Rath', 'ida60@example.com', '$2y$10$2wUz2tLD97ffvMt8PkXsDu/SRKJi3Ls4P5qAxZ4hAYvF5jwbrOFcW', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(68, 1, 'Elyse Hayes', 'Velda Hermiston', 'skiles.henri@example.org', '$2y$10$xQBZt4tZOaAaQ.ap6IwPsecvqepULelugwlZX282MANB7mw0DwZ3y', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(69, 1, 'Prof. Bernie Lindgren Sr.', 'Reid Pollich DVM', 'sparisian@example.org', '$2y$10$QAfmeUPJhhW2mdgN8FssrebDphY3RQt6BUxXTHQ.knDNyGpXgq93u', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(70, 1, 'Stevie Crooks DVM', 'Scot Grimes', 'zdoyle@example.net', '$2y$10$qDRWLoAQKTxovIrjnjlXseXHL/HObgYC4igY9MqZlFb.5IdXzfjTi', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(71, 1, 'Carolanne Williamson', 'Ms. Ellen Rempel DDS', 'carmelo.schultz@example.net', '$2y$10$Jp.GXS2g70t1tclqj4LYkO2GuZnZ/gpwFwSXbxSpsIS7fcsvfyyTq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(72, 1, 'Shannon Koch', 'Mr. Freddie Purdy', 'etha90@example.net', '$2y$10$jiLfJLMs9gIBD0Iy99iKe.73OFoKCoOeK.EvNKFVsoQ6/59wmKznK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(73, 1, 'Prof. Guillermo Rolfson', 'Mr. Braulio Stroman PhD', 'april15@example.org', '$2y$10$7yPYB1jaYu9Ncrmox1Na6eLBoXL36p/9bJitpiHMhwdkiFB4ACHca', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(74, 1, 'Erling Mertz', 'Annalise Fritsch', 'wolf.glenda@example.net', '$2y$10$3XaOrRCmxUqbk5dz5bwcb.biutbi7KaTCfJ.d7MfpY.EVnISCDu7S', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(75, 1, 'Ward Wunsch', 'Lindsey Skiles', 'eichmann.brisa@example.com', '$2y$10$4gGy8JavitLP9X6l2oN2guPGluCmXOmw2/QM8sDuHf3IhoAAXCtXi', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(76, 1, 'Dr. Mateo Adams IV', 'Laverne Friesen Sr.', 'ohoppe@example.net', '$2y$10$xPc/80QNJoqbMShL8NaoouJNFPjOi/TKCIrEfoR3.rvBnqGLIsMou', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(77, 1, 'Dr. Vincenzo Streich II', 'Olga Purdy Jr.', 'jgaylord@example.net', '$2y$10$apgHDa12wxdcOGRvt9hB8.E8kjXkOOAig.XSz.jv0aRZRc5hkw44y', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(78, 1, 'Bessie Berge', 'Johnny Gutmann', 'uparker@example.org', '$2y$10$Aq7pq6cO1lKDkPS7YhDfme.zFgEroWhW3/me2VEPSy68NUiFnJw6e', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(79, 1, 'Austin Wiza', 'Rory Howe IV', 'marguerite.langworth@example.org', '$2y$10$aH40rIC8BWGAYnFKWJOIvOQwNSmMSS3AZv3ujk9p604xC.QSj8aFq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(80, 1, 'Alfonso Hill PhD', 'Gladyce Lebsack I', 'czulauf@example.com', '$2y$10$S7xZn8rAIEKMwJh6aO8LXukUnk/UoVMYqALfWdkTtSakZOrCEIi3u', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(81, 1, 'Jasper Mertz', 'Jake Harvey', 'kamren.boyle@example.com', '$2y$10$iwko.7XGS46G8EWqZeT0hO0w2N1pSSa41WPgDiTvfJF.vImg/tCwK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(82, 1, 'Mr. Fern Hickle Sr.', 'Dr. Estrella Hartmann I', 'onicolas@example.net', '$2y$10$zflxjeSo2bguZ22/gNBCAO01XnDoldu4z6ERyhg1grgF1kYT2sdj2', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(83, 1, 'Prof. Chris Corwin', 'Miss Dejah Cruickshank', 'mireille10@example.org', '$2y$10$znIyOQFFWYThiwNw9.y3Hu.79LRmFpFnfmx2buFCl/zhC2qk2dLh6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(84, 1, 'Rigoberto Oberbrunner', 'Lloyd Trantow', 'huel.matilda@example.com', '$2y$10$YuXV98XhrMsmoXHoa/.pseXjv8O78ioBKsRoQXXEmVEbxXBCLi40K', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(85, 1, 'Savannah Schmeler', 'Maiya Baumbach', 'cgusikowski@example.org', '$2y$10$OE9.YAAE6NDTKflVUYtl5.dr5IoBnGEulAxoc/Rev1G7yDRTOd5TK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(86, 1, 'Kasey Crist', 'Antoinette Schaden IV', 'syost@example.net', '$2y$10$FH2ceqYyNoJ98C3m9xy/6uGbKg.oOUojyHhPpwGcskc3RJ4LXEuCa', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(87, 1, 'Faye Beier', 'Miss Blanca Abernathy', 'pouros.dallin@example.com', '$2y$10$6JDvCQflyTbwKSvAUa2CTOscAhx55lKhdMTOp7nFvZLL6//y6FZ0q', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(88, 1, 'Consuelo Yost', 'Greg Huel', 'ywalter@example.com', '$2y$10$ey1wAQsRYEgDHRVJaJp9ZeoIo.lZWruDtrMo.giC4dR1oxByzk10S', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(89, 1, 'Bryana Effertz', 'Camron Kuhic', 'djacobi@example.org', '$2y$10$QjyTB8T6VLf38qj2k4KPN.Jh9pSejQn0YNrd2.Skbbe8tQPy8mX0.', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(90, 1, 'Prof. Freda Rau IV', 'Demetris Fisher', 'zachery.jakubowski@example.com', '$2y$10$M/Gm/5qHn.62wj9PKtJ8jOv5MsYlNP1IPhArRkVk7LAru/HsJL92i', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(91, 1, 'Prof. Alvis Stiedemann', 'Marta Douglas', 'ehackett@example.org', '$2y$10$FskNG36uP05ZN6d0xkdUdOWx5uJRydlE5e4S5h4Sh0Mncsxy6a9A6', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(92, 1, 'Miss Paige Vandervort Jr.', 'Prof. Morton Mraz V', 'acollier@example.net', '$2y$10$TXkSbQMmOvAgDCzwr4uyQu/n9wYl9wl.yDSxJLtmoWRG7QSI0aaBm', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(93, 1, 'Alek O\'Keefe', 'Nellie Kuvalis', 'anais10@example.net', '$2y$10$OoixyW12Hh59eREhArWSj.bhqP74Rc2bK6JhfhXWBxoJkUzte.kaq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(94, 1, 'Abigale Muller', 'Providenci Wilkinson', 'katharina17@example.com', '$2y$10$.JfkeY8T1E1OhuHdiUKqJemw/XE5/6dYRVwIcia0/2XGJPtF5skru', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(95, 1, 'Toney Ankunding IV', 'Dr. Sandrine Daniel II', 'shanon76@example.org', '$2y$10$2EMGt/ayiS0h9ZrXGvwl5eisBnlCom8GvG4E0TrwAeZ.XpVTj5g5q', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(96, 1, 'Naomi Osinski', 'Hermann Wolf PhD', 'ujerde@example.net', '$2y$10$YE5pOQbaegZ8MndaGwBMM.YEY4BbilTcLgWzSKeYs2bt17vGKg0UK', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(97, 1, 'Prof. Iva Satterfield', 'Tillman Howell II', 'jesus.jones@example.org', '$2y$10$pU3cTk3SSwBGWdJygHVunOfvo0dYYEJTszo68MWK6NCcpjwVrEaru', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(98, 1, 'Darren Raynor IV', 'Kole Schneider', 'bruen.hoyt@example.net', '$2y$10$/6O.JSmTOhLXXh.RugBRruQTAsrqkEtQ7JuTKWTVm2Q9s3EzQJ6Dy', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(99, 1, 'Hardy Casper', 'Miss Linda Rohan', 'dallin.stracke@example.com', '$2y$10$ioyqr1KYMubMg9Jrb9fx.uCfWWB4X9bCf7VaaTZE8MUEVXXufVW4S', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15'),
(100, 1, 'Dameon Krajcik', 'Sunny Durgan', 'giles80@example.net', '$2y$10$1gqtK4WK77cDFc03RR8KzuDtKkq/49JFN8Cc/bR9Vpw63B6oTvTnq', NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, '2021-08-10 11:01:15', '2021-08-10 11:01:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_banners`
--

CREATE TABLE `m_banners` (
  `banner_id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED DEFAULT NULL,
  `banner_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt_img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_catalogs`
--

CREATE TABLE `m_catalogs` (
  `catalog_id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `catalog_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_messages`
--

CREATE TABLE `m_messages` (
  `message_id` bigint(20) UNSIGNED NOT NULL,
  `room_member_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nội dung tin nhắn',
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_products`
--

CREATE TABLE `m_products` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `catalog_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_author` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hot_flg` tinyint(4) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `m_shops`
--

CREATE TABLE `m_shops` (
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `url_alias` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tên cửa hàng',
  `shop_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'tên cửa hàng',
  `host_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'chủ sở hữu',
  `path_images` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tên cửa hàng',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'số điện thoại',
  `shop_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ cửa hàng',
  `is_active` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa active; 1: đã active',
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `pwr_id` bigint(20) UNSIGNED NOT NULL,
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room_chat`
--

CREATE TABLE `room_chat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room_chats`
--

CREATE TABLE `room_chats` (
  `room_chat_id` bigint(20) UNSIGNED NOT NULL,
  `room_chat_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `host_id` bigint(20) UNSIGNED NOT NULL,
  `del_flg` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: chưa xóa; 1: đã xóa',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room_members`
--

CREATE TABLE `room_members` (
  `room_member_id` bigint(20) UNSIGNED NOT NULL,
  `room_chat_id` bigint(20) UNSIGNED NOT NULL,
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `session_data`
--

CREATE TABLE `session_data` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cache_data`
--
ALTER TABLE `cache_data`
  ADD UNIQUE KEY `cache_data_key_unique` (`key`);

--
-- Chỉ mục cho bảng `maccount`
--
ALTER TABLE `maccount`
  ADD PRIMARY KEY (`account_id`),
  ADD UNIQUE KEY `UK_nq422wrbo6npsgggjc7dc9y7o` (`mail_adr`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `m_accounts`
--
ALTER TABLE `m_accounts`
  ADD PRIMARY KEY (`account_id`),
  ADD UNIQUE KEY `m_accounts_mail_adr_unique` (`mail_adr`);

--
-- Chỉ mục cho bảng `m_banners`
--
ALTER TABLE `m_banners`
  ADD PRIMARY KEY (`banner_id`),
  ADD KEY `m_banners_shop_id_foreign` (`shop_id`);

--
-- Chỉ mục cho bảng `m_catalogs`
--
ALTER TABLE `m_catalogs`
  ADD PRIMARY KEY (`catalog_id`),
  ADD KEY `m_catalogs_shop_id_foreign` (`shop_id`),
  ADD KEY `m_catalogs_parent_id_foreign` (`parent_id`);

--
-- Chỉ mục cho bảng `m_messages`
--
ALTER TABLE `m_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `m_messages_room_member_id_foreign` (`room_member_id`);

--
-- Chỉ mục cho bảng `m_products`
--
ALTER TABLE `m_products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `m_products_catalog_id_foreign` (`catalog_id`);

--
-- Chỉ mục cho bảng `m_shops`
--
ALTER TABLE `m_shops`
  ADD PRIMARY KEY (`shop_id`),
  ADD KEY `m_shops_host_id_foreign` (`host_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`pwr_id`),
  ADD KEY `password_resets_account_id_foreign` (`account_id`);

--
-- Chỉ mục cho bảng `room_chat`
--
ALTER TABLE `room_chat`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `room_chats`
--
ALTER TABLE `room_chats`
  ADD PRIMARY KEY (`room_chat_id`),
  ADD KEY `room_chats_host_id_foreign` (`host_id`);

--
-- Chỉ mục cho bảng `room_members`
--
ALTER TABLE `room_members`
  ADD PRIMARY KEY (`room_member_id`),
  ADD KEY `room_members_room_chat_id_foreign` (`room_chat_id`),
  ADD KEY `room_members_account_id_foreign` (`account_id`);

--
-- Chỉ mục cho bảng `session_data`
--
ALTER TABLE `session_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_data_user_id_index` (`user_id`),
  ADD KEY `session_data_last_activity_index` (`last_activity`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `m_accounts`
--
ALTER TABLE `m_accounts`
  MODIFY `account_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT cho bảng `m_banners`
--
ALTER TABLE `m_banners`
  MODIFY `banner_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `m_catalogs`
--
ALTER TABLE `m_catalogs`
  MODIFY `catalog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `m_messages`
--
ALTER TABLE `m_messages`
  MODIFY `message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `m_products`
--
ALTER TABLE `m_products`
  MODIFY `product_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `m_shops`
--
ALTER TABLE `m_shops`
  MODIFY `shop_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `pwr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `room_chat`
--
ALTER TABLE `room_chat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `room_chats`
--
ALTER TABLE `room_chats`
  MODIFY `room_chat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `room_members`
--
ALTER TABLE `room_members`
  MODIFY `room_member_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `m_banners`
--
ALTER TABLE `m_banners`
  ADD CONSTRAINT `m_banners_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `m_shops` (`shop_id`);

--
-- Các ràng buộc cho bảng `m_catalogs`
--
ALTER TABLE `m_catalogs`
  ADD CONSTRAINT `m_catalogs_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `m_catalogs` (`catalog_id`),
  ADD CONSTRAINT `m_catalogs_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `m_shops` (`shop_id`);

--
-- Các ràng buộc cho bảng `m_messages`
--
ALTER TABLE `m_messages`
  ADD CONSTRAINT `m_messages_room_member_id_foreign` FOREIGN KEY (`room_member_id`) REFERENCES `room_members` (`room_member_id`);

--
-- Các ràng buộc cho bảng `m_products`
--
ALTER TABLE `m_products`
  ADD CONSTRAINT `m_products_catalog_id_foreign` FOREIGN KEY (`catalog_id`) REFERENCES `m_catalogs` (`catalog_id`);

--
-- Các ràng buộc cho bảng `m_shops`
--
ALTER TABLE `m_shops`
  ADD CONSTRAINT `m_shops_host_id_foreign` FOREIGN KEY (`host_id`) REFERENCES `m_accounts` (`account_id`);

--
-- Các ràng buộc cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD CONSTRAINT `password_resets_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `m_accounts` (`account_id`);

--
-- Các ràng buộc cho bảng `room_chats`
--
ALTER TABLE `room_chats`
  ADD CONSTRAINT `room_chats_host_id_foreign` FOREIGN KEY (`host_id`) REFERENCES `m_accounts` (`account_id`);

--
-- Các ràng buộc cho bảng `room_members`
--
ALTER TABLE `room_members`
  ADD CONSTRAINT `room_members_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `m_accounts` (`account_id`),
  ADD CONSTRAINT `room_members_room_chat_id_foreign` FOREIGN KEY (`room_chat_id`) REFERENCES `room_chats` (`room_chat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
